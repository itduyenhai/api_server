const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt

const User = require('../api/v1/models/user')
const cfg = require('./main')

module.exports = (passport) => {
  const opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeader()
  opts.secretOrKey = cfg.secret

  passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
    User.findOne({
      id: jwt_payload.sub
    }, (err, user) => {
      if (err) {
        return done(err, false);
      }
      if (user) {
        done(null, user);
      } else {
        done(null, false);
        // or you could create a new account
      }
    })
  }))
}
