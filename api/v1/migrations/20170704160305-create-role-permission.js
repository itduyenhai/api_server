'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('RolePermissions', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      roleId: {
        type: Sequelize.UUID
      },
      permissionId: {
        type: Sequelize.UUID
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('RolePermissions');
  }
};
