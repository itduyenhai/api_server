'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('Profiles', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      cmnd: {
        type: Sequelize.INTEGER
      },
      ho_ten: {
        type: Sequelize.STRING
      },
      ngay_sinh: {
        type: Sequelize.DATEONLY
      },
      gioi_tinh: {
        type: Sequelize.BOOLEAN
      },
      dk_khai_sinh: {
        type: Sequelize.STRING
      },
      que_quan: {
        type: Sequelize.STRING
      },
      dan_toc: {
        type: Sequelize.STRING
      },
      ton_giao: {
        type: Sequelize.STRING
      },
      quoc_tich: {
        type: Sequelize.STRING
      },
      hon_nhan: {
        type: Sequelize.STRING
      },
      thuong_tru: {
        type: Sequelize.STRING
      },
      noi_o: {
        type: Sequelize.STRING
      },
      nhom_mau: {
        type: Sequelize.STRING
      },
      qh_gia_dinh: {
        type: Sequelize.UUID
      },
      qh_chu_ho: {
        type: Sequelize.UUID
      },
      chet: {
        type: Sequelize.DATEONLY
      },
      mat_tich: {
        type: Sequelize.DATEONLY
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('Profiles');
  }
};
