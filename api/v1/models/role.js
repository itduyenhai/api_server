'use strict'

module.exports = function(sequelize, DataTypes) {
  var Role = sequelize.define('Role', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    name: {
      type: DataTypes.STRING,
      validate: {
        // Validate Unique
        isUnique: function(value, next) {
          var self = this;
          Role.find({
              where: {
                name: value
              }
            })
            .then(function(role) {
              if (role && self.id !== role.id) {
                return next('Vai trò đã tồn tại');
              }
              return next();
            })
            .catch(function(err) {
              return next(err);
            });
        }
      }
    },
    slug: {
      type: DataTypes.STRING,
      validate: {
        isAlpha: {
          msg: 'Tên hiển thị vai trò là chữ thường'
        },
        // Validate Unique
        isUnique: function(value, next) {
          var self = this;
          Role.find({
              where: {
                slug: value
              }
            })
            .then(function(role) {
              if (role && self.id !== role.id) {
                return next('Tên hiển thị vai trò đã tồn tại');
              }
              return next();
            })
            .catch(function(err) {
              return next(err);
            });
        }
      }
    },
    description: {
      type: DataTypes.STRING
    }
  }, {
    timestamps: false,
    underscored: true,
    classMethods: {
      associate: function(models) {
        Role.hasMany(models.User);
        Role.belongsToMany(models.Permission, {through: 'RolePermission'});
      }
    }
  })

  return Role;
};
