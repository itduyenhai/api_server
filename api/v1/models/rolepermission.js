'use strict';
module.exports = function(sequelize, DataTypes) {
  var RolePermission = sequelize.define('RolePermission', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    roleId: DataTypes.UUID,
    permissionId: DataTypes.UUID
  }, {
    underscored: true,
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return RolePermission;
};
