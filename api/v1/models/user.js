'use strict'
const bcrypt = require('bcryptjs');

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },    
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    displayName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
      validate: {
        isEmail: {
          msg: 'Email không đúng định dạng'
        },
        isUnique: function(value, next) {
          var self = this;
          User.find({
              where: {
                email: value
              }
            })
            .then(function(user) {
              if (user && self.id !== user.id) {
                return next('Email đã tồn tại');
              }
              return next();
            })
            .catch(function(err) {
              return next(err);
            });
        }
      },
    },
    password: DataTypes.STRING,
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    token: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    underscored: true,
    paranoid: true,
    classMethods: {
      associate: function(models) {
        User.belongsTo(models.Role)
      },
    },
    hooks: {
      beforeCreate: (user) => {
        user.password = bcrypt.hashSync(user.password, 8)
      }
    }
  });
  return User;
};
