'use strict';
module.exports = function(sequelize, DataTypes) {
  var Permission = sequelize.define('Permission', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    name: {
      type: DataTypes.STRING,
      validate: {
        // Validate Unique
        isUnique: function(value, next) {
          var self = this;
          Permission.find({
              where: {
                name: value
              }
            })
            .then(function(permission) {
              if (permission && self.id !== permission.id) {
                return next('Quyền hạn đã tồn tại');
              }
              return next();
            })
            .catch(function(err) {
              return next(err);
            });
        }
      }
    },
    slug: {
      type: DataTypes.STRING,
      validate: {
        is: {
          args: /^[a-z0-9]+(?:-[a-z0-9]+)*$/,
          msg: 'Tên hiển thị quyền hạn là chữ thường'
        },
        // Validate Unique
        isUnique: function(value, next) {
          var self = this;
          Permission.find({
              where: {
                slug: value
              }
            })
            .then(function(permission) {
              if (permission && self.id !== permission.id) {
                return next('Tên hiển thị quyền hạn đã tồn tại');
              }
              return next();
            })
            .catch(function(err) {
              return next(err);
            });
        }
      }
    },
    description: DataTypes.STRING
  }, {
    timestamps: false,
    underscored: true,
    classMethods: {
      associate: function(models) {
        Permission.belongsToMany(models.Role, {through: 'RolePermission'});
      }
    }
  });
  return Permission;
};
