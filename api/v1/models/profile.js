'use strict';
module.exports = function(sequelize, DataTypes) {
  var Profile = sequelize.define('Profile', {
    cmnd: DataTypes.INTEGER,
    ho_ten: DataTypes.STRING,
    ngay_sinh: DataTypes.DATE,
    gioi_tinh: DataTypes.BOOLEAN,
    dk_khai_sinh: DataTypes.STRING,
    que_quan: DataTypes.STRING,
    dan_toc: DataTypes.STRING,
    ton_giao: DataTypes.STRING,
    quoc_tich: DataTypes.STRING,
    hon_nhan: DataTypes.STRING,
    thuong_tru: DataTypes.STRING,
    noi_o: DataTypes.STRING,
    nhom_mau: DataTypes.STRING,
    qh_gia_dinh: DataTypes.UUID,
    qh_chu_ho: DataTypes.UUID,
    chet: DataTypes.DATE,
    mat_tich: DataTypes.DATE
  }, {
    underscored: true,
    paranoid: true,
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Profile;
};
