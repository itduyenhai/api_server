const User = require('../models/').User
const Role = require('../models/').Role
const Permission = require('../models/').Permission

module.exports = {
  // List
  list(req, res) {
    Role.findAll().then(role =>{
      res.status(200).json(role)
    }). catch (err =>{
      res.status(500).json({msg: 'Không tìm thấy vai trò'})
    })
  },
  // Create
  create(req, res) {
    if (!req.body.name || !req.body.slug ) {
      res.status(401).json({msg: 'Nhập thông tin vai trò'})
    } else {
      Role.create({
        name: req.body.name,
        slug: req.body.slug,
        description: req.body.description
      })
      .then(role => {
        res.status(200).json(role)
      })
      .catch(err => {
        for (var i in err.errors) {
          res.status(500).json(err.errors);
        }
      })
    }
  },

  // Show
  show(req, res) {
    res.status(200).json({msg: 'Find role by id'})
  },
  // Update
  update(req, res) {
    res.status(200).json({msg: 'Update role'})
  },
  // Delete
  delete(req, res) {
    res.status(200).json({msg: 'Delete role'})
  }
}
