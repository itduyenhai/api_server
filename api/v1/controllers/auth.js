const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const cfg = require('../../../config/main')
const User = require('../models/').User

module.exports = {
  // Login
  login(req, res) {
    User.findOne({
        where: {
          email: req.body.email
        }
      })
      .then(user => {
        bcrypt.compare(user.password, req.body.password, (err) => {
          if (err) {
            res.status(400).json({
              msg: 'Kiểm tra lại mật khẩu'
            })
          } else {
            var token = jwt.sign({
              user
            }, cfg.secret, {
              expiresIn: 3600
            });
            res.status(200).json({
              msg: 'OK',
              userId: user.id,
              token: token
            })
          }
        })
      })
      .catch(err => {
        res.status(401).json({
          msg: 'Tài khoản không tồn tại'
        })
      });
  },

  // Register
  register(req, res) {
    if (!req.body.email || !req.body.password) {
      res.status(500).json({
        msg: 'Nhập thông tin đăng ký'
      })
    } else {
      User.create({
          username: req.body.username,
          email: req.body.email,
          password: req.body.password,
          // active: req.body.active
        })
        .then(user => {
          res.status(200).json(user);
        })
        .catch(err => {
          for (var i in err.errors) {
            res.status(500).json(err.errors[0].message);
          }
        });
    }
  }
}
