const Role = require('../models/').Role
const Permission = require('../models/').Permission

module.exports = {
  // List
  list(req, res) {
    Permission.findAll().then(permission =>{
      res.status(200).json(permission)
    }). catch (err =>{
      res.status(500).json({msg: 'Không tìm thấy quyền hạn'})
    })
  },
  // Create
  create(req, res) {
    if (!req.body.name || !req.body.slug ) {
      res.status(401).json({msg: 'Nhập thông tin quyền hạn'})
    } else {
      Permission.create({
        name: req.body.name,
        slug: req.body.slug,
        description: req.body.description
      })
      .then(permission => {
        res.status(200).json(permission)
      })
      .catch(err => {
        for (var i in err.errors) {
          res.status(500).json(err.errors);
        }
      })
    }
  },

  // Show
  show(req, res) {
    res.status(200).json({msg: 'Find Permission by id'})
  },
  // Update
  update(req, res) {
    res.status(200).json({msg: 'Update Permission'})
  },
  // Delete
  delete(req, res) {
    res.status(200).json({msg: 'Delete Permission'})
  }
}
