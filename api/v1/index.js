const router = require('express').Router()

// require Controller
const userCtrl = require('./controllers/user')
const roleCtrl = require('./controllers/role')
const permissionCtrl = require('./controllers/permission')

// User
router.get('/user', userCtrl.index)

// Role
router.get('/role', roleCtrl.list)
router.post('/role/create', roleCtrl.create)
router.get('/role/:id', roleCtrl.show)
router.put('/role/:id', roleCtrl.update)
router.delete('/role/:id', roleCtrl.delete)

// Permission
router.get('/permission', permissionCtrl.list)
router.post('/permission/create', permissionCtrl.create)
router.get('/permission/:id', permissionCtrl.show)
router.put('/permission/:id', permissionCtrl.update)
router.delete('/permission/:id', permissionCtrl.delete)

module.exports = router;
