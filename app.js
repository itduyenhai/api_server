const express = require('express')
const bodyParser = require('body-parser')
const passport = require('passport')

// Var Route
const v1 = require('./api/v1')
const app = express()
const port = 3000

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

// Passport init
// app.use(passport.initialize())

// Auth
const authCtrl = require('./api/v1/controllers/auth')
app.post('/api/v1/login', authCtrl.login)
app.post('/api/v1/register', authCtrl.register)

// Passport Authenticate
// app.use(passport.authenticate())

// Use Route
app.use('/api/v1/', v1)

app.listen(port, () => {
    console.log('API server running on port: ' + port)
})

module.exports = app
