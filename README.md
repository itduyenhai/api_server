# README #

API server cấp vai trò, phân quyền cho người dùng

### Tác giả ###

* Name: Nguyễn Văn Vũ
* Email: vunguyen.catv@gmail.com
* Phone: 01234.84.4567
* Version: 1.0

### Chức năng ###
1. User - Role: quan hệ 1-n
2. Role - Permission: quan hệ n-n
3. Cơ sở dữ liệu: Postgres + Sequelize ORM
